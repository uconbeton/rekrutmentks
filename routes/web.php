<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::group(['middleware' => ['auth']], function () {
//     Route::get('/home', function () {
//         return view('admin.form');
//     })->name('home');
    
//     Route::group(['middleware' => ['permission:edit articles']], function (){
//         Route::get('write', 'AfganController@store');
//     });

//     Route::get('publish', 'AfganController@update');
//     Route::get('unpublish', 'AfganController@update');
//     Route::get('delete', 'AfganController@destroy');
// });


Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/user', 'AdminController@index');
        Route::get('/terima/{no}', 'AdminController@terima');
        Route::get('/tolak/{no}', 'AdminController@tolak');
        Route::get('/userterima', 'AdminController@indexterima');
        Route::get('/usertolak', 'AdminController@indextolak');
        Route::get('/detail_peserta/{id?}', 'AdminController@detail');
        Route::get('/detail_pesertacek/{id?}', 'AdminController@detailcek');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/us', 'MasterController@us');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/usterima', 'MasterController@usterima');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::post('/usterimadata', 'MasterController@usterimadata');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::post('/ustolakdata', 'MasterController@ustolakdata');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/ustolak', 'MasterController@ustolak');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/has', 'MasterController@has');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/set', 'MasterController@set');
    });
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::post('/import', 'MasterController@import')->name('import');
    });
    
    
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/hasil', 'MasterController@hasiltes');
    });

    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/setting', 'MasterController@setting');
    });

    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/fhasil', 'MasterController@formhasil');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/peserta', 'MasterController@formpeserta');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/pengalaman', 'MasterController@formpengalaman');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/akademik', 'MasterController@formakademik');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/dp', 'MasterController@domprov');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/dk/{id?}', 'MasterController@domkab');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/dkec/{id?}', 'MasterController@domkec');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/dkel/{id?}', 'MasterController@domkel');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/kp', 'MasterController@ktpprov');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/kk/{id?}', 'MasterController@ktpkab');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/kkec/{id?}', 'MasterController@ktpkec');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/kkel/{id?}', 'MasterController@ktpkel');
    });
    
    Route::group(['middleware' => ['role:admin']], function (){
        Route::post('/inspeserta/', 'MasterController@insertpeserta');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::post('/insakademik/', 'MasterController@insertakademik');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::post('/insjadwal/', 'MasterController@insertjadwal');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::post('/inspengalaman/', 'MasterController@inserpengalaman');
    });

    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/jadwal/', 'MasterController@jadwal');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/listjadwal/{act}', 'MasterController@listjadwal');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/formjadwal/{not}/{act?}', 'MasterController@formjadwal');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/djadwal/{act?}', 'MasterController@deletejadwal');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/prin/{act?}', 'MasterController@prinjadwal');
    });
    
    // Route::group(['middleware' => ['role:admin']], function (){
    //     Route::get('/', 'MasterController@index');
    //     Route::get('/index', 'MasterController@index');
    //     Route::get('/home', 'MasterController@index');
    // });

    Route::get('/', 'MasterController@index');
    Route::get('/index', 'MasterController@index');
    Route::get('/home', 'MasterController@index');

    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/indexadmin', 'AdminController@index');
        
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/index', 'MasterController@index');
    });
    
   
    Route::group(['middleware' => ['role:superadmin']], function (){
        Route::get('/deletesetting/{id?}', 'MasterController@deleteset');
    });
    Route::group(['middleware' => ['role:admin']], function (){
        Route::get('/formuser', 'AdminController@formuser');
    });
    Route::group(['middleware' => ['role:useraplikasi']], function (){
        Route::get('/users', 'AdminController@index');
    });
   

    // Route::group(['prefix' =>'hci','middleware'    => ['permission:akses hci']],function(){
    //         Route::get('/', function () {
    //                 return view('hci.index');
    //             });
    //         Route::get('/deltim/{id}', 'ObyekController@deltim');
        
    // });
});

Auth::routes();

