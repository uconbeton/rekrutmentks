<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahapan extends Model
{
    protected $table = 'tahapan';
    public $timestamps = false;

    public function hasiltes(){
        return $this->hasMany('App\HasilTes');
    }
}
