<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    public $timestamps = false;

    public function domkec(){
        return $this->hasMany('App\Peserta','id_kec','dom_kec_id');
    }

    public function ktpkec(){
        return $this->hasMany('App\Peserta','id_kec','ktp_kec_id');
    }
}
