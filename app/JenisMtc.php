<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisMtc extends Model
{
    protected $table = 'jenis_mtc';
    public $timestamps = false;
}
