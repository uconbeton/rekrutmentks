<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesin extends Model
{
    protected $table = 'mesin';
    public $timestamps = false;

    public function jadwal(){
        return $this->hasMany('App\Jadwal');
    }

}
