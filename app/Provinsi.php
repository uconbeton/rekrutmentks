<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    public $timestamps = false;

    public function domprov(){
        return $this->hasMany('App\Peserta','id_prov','dom_prov_id');
    }

    public function ktpprov(){
        return $this->hasMany('App\Peserta','id_prov','ktp_prov_id');
    }
}
