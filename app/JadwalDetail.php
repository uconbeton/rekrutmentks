<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalDetail extends Model
{
    protected $table = 'jadwal_detail';
    public $timestamps = false;
}
