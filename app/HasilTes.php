<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilTes extends Model
{
    protected $table = 'hasiltes';
    public $timestamps = false;
    protected $fillable = ['users_id','no_tes','email','tahapan_id','tanggal','tanggal_tes','tempat_tes','jam_tes','not'];

    public function thp()
    {
        return $this->belongsTo('App\Tahapan','tahapan_id','id');
    }
    

    public function scopeGroupByTanggal($query)
    {
        $query->select('tanggal')
            ->groupBy('tanggal');
    } 
}
