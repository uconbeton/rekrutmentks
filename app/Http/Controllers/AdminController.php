<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Peserta;
use App\HasRole;
use App\Pengalaman;

class AdminController extends Controller
{
    public function index()
    {
       $user=User::all();
       $total=User::count();
       $lengkap=Peserta::where('no_hp','!=','')->count();
       return view('admin.user',compact('user','total','lengkap'));
    }

    public function indexterima()
    {
       $user=User::all();
       $total=Peserta::where('no_hp','!=','')->count();
       $diterima=Peserta::where('no_hp','!=','')->where('penerimaan','1')->count();
       $ditolak=Peserta::where('no_hp','!=','')->where('penerimaan','2')->count();
       return view('admin.diterima',compact('user','total','diterima','ditolak'));
    }

    public function indextolak()
    {
       $user=User::all();
       $total=Peserta::where('no_hp','!=','')->count();
       $diterima=Peserta::where('no_hp','!=','')->where('penerimaan','1')->count();
       $ditolak=Peserta::where('no_hp','!=','')->where('penerimaan','2')->count();
       return view('admin.ditolak',compact('user','total','diterima','ditolak'));
    }
    public function detail($id=null)
    {
       $data=Peserta::where('users_id',$id)->first();
       $pengalaman=Pengalaman::where('users_id',$id)->get();
       return view('admin.detail_peserta',compact('data','pengalaman'));
    }

    public function detailcek($id=null)
    {
       $data=Peserta::where('users_id',$id)->first();
       $pengalaman=Pengalaman::where('users_id',$id)->get();
       return view('admin.detail_pesertacek',compact('data','pengalaman'));
    }
    public function formuser()
    {
        $user="";
       return view('admin.formuser',compact('user'));
    }

    public function terima($no){
        $data                       =Peserta::where('no_ktp',$no)->first();
        $data->penerimaan           =1;
        $data->save();

        return redirect('/user');
    }

    public function tolak($no){
        $data                       =Peserta::where('no_ktp',$no)->first();
        $data->penerimaan           =2;
        $data->save();

        return redirect('/user');
    }
}
