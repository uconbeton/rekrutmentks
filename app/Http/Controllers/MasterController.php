<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
//use App\Exports\UsersExport;
use App\Imports\HasilImport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
use App\User;
use App\Peserta;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Kelurahan;
use App\HasilTes;
use App\Agama;
use App\Pengalaman;
use App\Akademik;
use DataTables;

class MasterController extends Controller
{
    public function index(){
    if(auth::user()->hasRole('superadmin')){
        return redirect('/user');
    }else{
            $tes=HasilTes::with(['thp'])->where('users_id',auth::user()->id)->orderBy('tahapan_id','desc')->get();
            $data=$tes->unique('tahapan_id');
            $peserta = collect([]);
            
            foreach($data as $datas){
                $dat=HasilTes::where('users_id',auth::user()->id)
                ->where('tahapan_id',$datas->tahapan_id)
                ->first();
            
                $peserta->push([
                    'not' => $dat->not,
                    'tempat_tes' => $dat->tempat_tes
                    
                ]);
            }

            $profil=Peserta::where('users_id',auth::user()->id)->first();
            $pengalaman=Pengalaman::where('users_id',auth::user()->id)->get();

            $birthday = "1992-05-22";
        
        
            $now = Carbon::now(); // Tanggal sekarang
            $b_day =Carbon::parse($profil->tgl_lahir); // Tanggal Lahir
            $umur = $b_day->diffInYears($now);  // Menghitung umur
            return view('index',compact('data','peserta','profil','pengalaman','umur'));
        }
    }

    public function formpeserta($act=null)
    {
       
        $data=Peserta::where('users_id',auth::user()->id)->first();
        $userid=auth::user()->id;
        $agama=Agama::all();
        $cek='edit';

      
       return view('master.formpeserta',compact('data','cek','act','userid','agama'));
    }

    public function formakademik($act=null)
    {
       
        $data=Akademik::where('users_id',auth::user()->id)->first();
        $userid=auth::user()->id;
        $cek='edit';

      
       return view('master.formakademik',compact('data','cek','act','userid'));
    }

    public function formpengalaman($act=null)
    {
       
        $data=Akademik::where('users_id',auth::user()->id)->first();
        $peserta=Peserta::where('users_id',auth::user()->id)->first();
        $pengalaman=Pengalaman::where('users_id',auth::user()->id)->get();
        $jumlah=Pengalaman::where('users_id',auth::user()->id)->count();
        $userid=auth::user()->id;
        $cek='edit';

      
       return view('master.formpengalaman',compact('data','cek','act','userid','pengalaman','jumlah','peserta'));
    }


    public function domprov(){
        $data=Provinsi::all();
        return view('master.popdomprov',compact('data'));
    }

    public function domkab($id=null){
        $data=Kabupaten::where('id_prov',$id)->get();
        return view('master.popdomkab',compact('data'));
    }

    public function domkec($id=null){
        $data=Kecamatan::where('id_kab',$id)->get();
        return view('master.popdomkec',compact('data'));
    }

    public function domkel($id=null){
        $data=Kelurahan::where('id_kec',$id)->get();
        return view('master.popdomkel',compact('data'));
    }

    public function ktpprov(){
        $data=Provinsi::all();
        return view('master.popktpprov',compact('data'));
    }

    public function ktpkab($id=null){
        $data=Kabupaten::where('id_prov',$id)->get();
        return view('master.popktpkab',compact('data'));
    }
    public function hasiltes(){
        return view('admin.peserta');
    }
    public function setting(){
        $tes=HasilTes::with(['thp'])->get();
        $user=$tes->unique('tahapan_id');


        $jumya = collect([]);
        $jumno = collect([]);
        foreach($user as $usernya){
            $ya=HasilTes::where('not','ya')
            ->where('tahapan_id',$usernya->tahapan_id)
            ->count();
        
            $jumya->push([
                'tahapan_id' => $usernya->tahapan_id,
                'count' => $ya
            ]);

            $no=HasilTes::where('not','no')
            ->where('tahapan_id',$usernya->tahapan_id)
            ->count();
        
            $jumno->push([
                'tahapan_id' => $usernya->tahapan_id,
                'count' => $no
            ]);
        }
       
        
        return view('admin.setting',compact('user','jumya','jumno'));
        
    }

    public function ktpkec($id=null){
        $data=Kecamatan::where('id_kab',$id)->get();
        return view('master.popktpkec',compact('data'));
    }
    public function deleteset($id=null){
        $data=HasilTes::where('tahapan_id',$id)->delete();
        return redirect('/setting');
    }

    public function ktpkel($id=null){
        $data=Kelurahan::where('id_kec',$id)->get();
        return view('master.popktpkel',compact('data'));
    }
    public function insertpeserta(request $request)
    {
       
            $rules = [
                'no_ktp'             => 'required|min:15',
                'dom_alamat'         => 'required',
                'sts_pernikahan'     => 'required',
                'agama_id'           => 'required',
                'tgl_lahir'          => 'required',
                'jenis_kelamin'      => 'required',
                'dom_prov_id'        => 'required',
                'filektp'            => 'mimes:jpg,gif,jpeg,png|max:200',
                'dom_kab_id'         => 'required',
                'dom_kec_id'         => 'required',
                'dom_kel_id'         => 'required',
                'ktp_prov_id'        => 'required',
                'ktp_kab_id'         => 'required',
                'ktp_kec_id'         => 'required',
                'ktp_kel_id'         => 'required',
                'ktp_alamat'         => 'required'
                
                
            ];
        
            $customMessages = [
                'filektp.mimes'               => '- File KTP harus berformat jpg,gif,jpeg,png',
                'filektp.max'                 => '- File KTP Maximal 200kb',
                'no_ktp.required'             => '- No KTP Harus diisi',
                'agama_id.required'           => '- Agama Harus diisi',
                'sts_pernikahan.required'     => '- Status Pernikahan Harus diisi',
                'tgl_lahir.required'          => '- Tanggal Lahir Harus diisi',
                'jenis_kelamin.required'      => '- Jenis Kelamin Harus diisi',
                'no_ktp.min'                  => '- No KTP Harus 15 karakter',
                'dom_alamat.required'         => '- Alamat Domisili Harus diisi',
                'dom_prov_id.required'        => '- Provinsi Domisili Harus diisi',
                'dom_kab_id.required'         => '- Kabupaten / Kota Domisili Harus diisi',
                'dom_kec_id.required'         => '- Kecamatan Domisili Harus diisi',
                'dom_kel_id.required'         => '- Kelurahan Domisili Harus diisi',
                'ktp_alamat.required'         => '- Alamat sesuai KTP Harus diisi',
                'ktp_prov_id.required'         => '- Provinsi Sesuai KTP Harus diisi',
                'ktp_kab_id.required'         => '- Kabupaten / Kota Sesuai KTP Harus diisi',
                'ktp_kec_id.required'         => '- Kecamatan Sesuai KTP Harus diisi',
                'ktp_kel_id.required'         => '- Kelurahan Sesuai KTP Harus diisi'

               
            ];
        
            
    
            $validator = Validator::make($request->all(), $rules,$customMessages); 
           
            if ($validator->fails()) {
                return redirect('/peserta/'.$request->id)
                            ->withErrors($validator)
                            ->withInput();
            }

            if(is_null($request->filektp)){
                if(is_null($request->filenya)){
                    $filektp = '';
                }else{
                    $filektp =$request->filenya;
                }
               
            }else{
                
                $type = $request->filektp->extension();
                $filektp = $request->filektp->storeAs('images',$request->no_ktp.'.'.$type);
            }
           
            $data                =Peserta::where('users_id',$request->userid)->first();
            $data->no_ktp        =$request->no_ktp;
            $data->sts_pernikahan  =$request->sts_pernikahan;
            $data->no_hp         =$request->no_hp;
            $data->agama_id      =$request->agama_id;
            $data->dom_alamat    =$request->dom_alamat;
            $data->tgl_lahir     =$request->tgl_lahir;
            $data->jenis_kelamin =$request->jenis_kelamin;
            $data->dom_prov_id   =$request->dom_prov_id;
            $data->dom_kab_id    =$request->dom_kab_id;
            $data->dom_kec_id    =$request->dom_kec_id;
            $data->dom_kel_id    =$request->dom_kel_id;
            $data->ktp_alamat    =$request->ktp_alamat;
            $data->ktp_prov_id   =$request->ktp_prov_id;
            $data->ktp_kab_id    =$request->ktp_kab_id;
            $data->ktp_kec_id    =$request->ktp_kec_id;
            $data->ktp_kel_id    =$request->ktp_kel_id;
            $data->filektp       =$filektp;
            $data->sts           =1;
            $data->save();

            
            
         return redirect('/akademik');
           
    }

    public function insertakademik(request $request)
    {
       
            $rules = [
                'pendidikan'          => 'required',
                'ipk'               => 'required',
                'nama_sekolah'       => 'required',
                'jurusan_sekolah'    => 'required',
                'filetranskip'        => 'mimes:jpg,gif,jpeg,png,pdf|max:200',
                'fileijazah'         => 'mimes:jpg,gif,jpeg,png,pdf|max:200'
                
                
            ];
        
            $customMessages = [
                'fileijazah.mimes'               => '- File Ijazah harus berformat jpg,gif,jpeg,png,pdf',
                'fileijazah.max'                 => '- File Ijazah Maximal 200kb',
                'filetranskip.mimes'             => '- File Trasnkip harus berformat jpg,gif,jpeg,png',
                'filetranskip.max'               => '- File Trasnkip Maximal 200kb',
                'ipk.required'                   => '- Nilai IPK Harus diisi',
                'pendidikan.required'             => '- Pendidikan Terakhir Harus diisi',
                'nama_sekolah.required'          => '- Nama Sekolah Harus diisi',
                'jurusan_sekolah.required'       => '- Jurusan Sekolah Harus diisi'
               
            ];
        
            
    
            $validator = Validator::make($request->all(), $rules,$customMessages); 
           
            if ($validator->fails()) {
                return redirect('/akademik/'.$request->id)
                            ->withErrors($validator)
                            ->withInput();
            }

            if(is_null($request->fileijazah)){
                if(is_null($request->filenyaijazah)){
                    $fileijazah = '';
                }else{
                    $fileijazah =$request->filenyaijazah;
                }
               
            }else{
                
                $type = $request->fileijazah->extension();
                $fileijazah = $request->fileijazah->storeAs('images','IJZ'.$request->no_ktp.'.'.$type);
            }

            if(is_null($request->filetranskip)){
                if(is_null($request->filenyatranskip)){
                    $filetranskip = '';
                }else{
                    $filetranskip =$request->filenyatranskip;
                }
               
            }else{
                
                $type = $request->filetranskip->extension();
                $filetranskip = $request->filetranskip->storeAs('images','TRN'.$request->no_ktp.'.'.$type);
            }
           
            $data                   =Akademik::where('users_id',$request->userid)->first();
            $data->ipk        =$request->ipk;
            $data->nama_sekolah     =$request->nama_sekolah;
            $data->pendidikan       =$request->pendidikan;
            $data->jurusan_sekolah  =$request->jurusan_sekolah;
            $data->fileijazah       =$fileijazah;
            $data->filetranskip     =$filetranskip;
            $data->sts              =1;
            $data->save();

            
            
         return redirect('/pengalaman');
           
    }

    public function inserpengalaman(request $request)
    {
       
            $rules = [
                'filecv'        => 'mimes:pdf|max:200'
                
                
            ];
        
            $customMessages = [
                'filecv.mimes'               => '- File Ijazah harus berformat Pdf',
                'filecv.max'                 => '- File Ijazah Maximal 200kb'
               
            ];
        
            
    
            $validator = Validator::make($request->all(), $rules,$customMessages); 
           
            if ($validator->fails()) {
                return redirect('/pengalaman/')
                            ->withErrors($validator)
                            ->withInput();
            }

            if(is_null($request->filecv)){
                if(is_null($request->filecv)){
                    $filecv = '';
                }else{
                    $filecv =$request->editfilecv;
                }
               
            }else{
                
                $type = $request->filecv->extension();
                $filecv = $request->filecv->storeAs('images','CV'.$request->no_ktp.'.'.$type);
            }

            
           
            $data                   =Peserta::where('users_id',$request->userid)->first();
            $data->filecv           =$filecv;
            $data->save();

            $delet=Pengalaman::where('users_id',$request->userid)->delete();
            for ($x=0;$x<sizeof($request->nama_perusahaan);$x++) {
                if($request->nama_perusahaan[$x]!=''){

                
                    $pengalaman                     = New Pengalaman;
                    $pengalaman->nama_perusahaan    = $request->nama_perusahaan[$x];
                    $pengalaman->jabatan            = $request->jabatan[$x];
                    $pengalaman->mulai              = $request->mulai[$x];
                    $pengalaman->sampai             = $request->sampai[$x];
                    $pengalaman->users_id           = $request->userid;
                    $pengalaman->selisih            = $request->selisih[$x];
                    $pengalaman->job1               = $request->job1[$x];
                    $pengalaman->job2               = $request->job2[$x];
                    $pengalaman->job3               = $request->job3[$x];
                    $pengalaman->job4               = $request->job4[$x];
                    $pengalaman->job5               = $request->job5[$x];
                    $pengalaman->save();
                }
            } 

            return redirect('/');
           
    }

    public function us(){
        $test=Peserta::with(['domprovs','ktpprovs','ktpkabs','ktpkecs','akademik'])->where('no_hp','!=','')->where('penerimaan',null)->orderBy('users_id','Asc')->get();
        return datatables()->of($test)->toJson();
    }

    public function usterima(){
        $test=Peserta::with(['domprovs','ktpprovs','ktpkabs','ktpkecs','akademik'])->where('no_hp','!=','')->where('penerimaan',1)->orderBy('users_id','Asc')->get();
        return datatables()->of($test)->toJson();
    }

    public function usterimadata(request $request){
        $jum=count($request->no_ktp);
        for($x=0;$x<$jum;$x++){
            $data               =Peserta::where('no_ktp',$request->no_ktp[$x])->first();
            $data->penerimaan   =1;
            $data->save();
        }
        
    }
    public function ustolakdata(request $request){
        $jum=count($request->no_ktp);
        for($x=0;$x<$jum;$x++){
            $data               =Peserta::where('no_ktp',$request->no_ktp[$x])->first();
            $data->penerimaan   =2;
            $data->save();
        }
        
    }
    public function ustolak(){
        $test=Peserta::with(['domprovs','ktpprovs','ktpkabs','ktpkecs','akademik'])->where('no_hp','!=','')->where('penerimaan',2)->orderBy('users_id','Asc')->get();
        return datatables()->of($test)->toJson();
    }
    public function has(){
        $test=HasilTes::with(['thp'])->orderBy('tahapan_id','desc')->get();
        return datatables()->of($test)->toJson();
    }

    public function set(){
        $test=HasilTes::with(['thp'])->get();

        // return DataTables::eloquent($test)
        //     ->addColumn('count', function(HasilTes $hasil) {
        //         $count = HasilTes::groupBy('tanggal')
        //             ->where('tanggal', $hasil->tanggal);
                
        //         return $count;
        // })
        // ->toJson();
        return datatables()->of($test->unique('tahapan_id')->all())->toJson();
    }
    
    
    //Import------------------------------------------------------------------------------
    public function import() 
    {
        // Excel::import(new HasilImport,request()->file('file'));
           
        // return back();
        Excel::import(new HasilImport, request()->file('file'));
            
        return back();
    }

    public function formhasil()
    {
       return view('admin.formhasil');
    }
//end------------------------------------------------------------------

}
