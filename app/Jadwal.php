<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    public $timestamps = false;

    public function mstmesin(){
        return $this->belongsTo('App\Mesin','mesin_id','id');
    }

}
