<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $table = 'peserta';
    public $timestamps = false;
    
    public function userss()
    {
        return $this->belongsTo('App\User','users_id','id');
    }
    public function akademik()
    {
        return $this->belongsTo('App\Akademik','users_id','users_id');
    }
    public function agama()
    {
        return $this->belongsTo('App\Agama','agama_id','id');
    }
    public function domprovs()
    {
        return $this->belongsTo('App\Provinsi','dom_prov_id','id_prov');
    }

    public function ktpprovs()
    {
        return $this->belongsTo('App\Provinsi','ktp_prov_id','id_prov');
    }

    public function domkabs()
    {
        return $this->belongsTo('App\Kabupaten','dom_kab_id','id_kab');
    }

    public function ktpkabs()
    {
        return $this->belongsTo('App\Kabupaten','ktp_kab_id','id_kab');
    }

    
    public function domkecs()
    {
        return $this->belongsTo('App\Kecamatan','dom_kec_id','id_kec');
    }

    public function ktpkecs()
    {
        return $this->belongsTo('App\Kecamatan','ktp_kec_id','id_kec');
    }

    public function domkels()
    {
        return $this->belongsTo('App\Kelurahan','dom_kel_id','id_kel');
    }

    public function ktpkels()
    {
        return $this->belongsTo('App\Kelurahan','ktp_kel_id','id_kel');
    }
    
}
