<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table = 'kabupaten';
    public $timestamps = false;

    public function domkab(){
        return $this->hasMany('App\Peserta','id_kab','dom_kab_id');
    }

    public function ktpkab(){
        return $this->hasMany('App\Peserta','id_kab','ktp_kab_id');
    }
}
