<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahan';
    public $timestamps = false;

    public function domkel(){
        return $this->hasMany('App\Peserta','id_kel','dom_kel_id');
    }

    public function ktpkel(){
        return $this->hasMany('App\Peserta','id_kel','ktp_kel_id');
    }
}
