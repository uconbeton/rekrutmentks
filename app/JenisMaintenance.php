<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisMaintenance extends Model
{
    protected $table = 'jenis_mtc';
    public $timestamps = false;
}
