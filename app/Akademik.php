<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akademik extends Model
{
    protected $table = 'akademik';
    public $timestamps = false;
    public function peserta(){
        return $this->hasMany('App\Peserta','users_id','users_id');
    }
}
