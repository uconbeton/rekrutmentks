<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasRole extends Model
{
    protected $table = 'model_has_roles';
    public $timestamps = false;

    public function userss()
    {
        return $this->belongsTo('App\HasRole','id','model_id');
    }
}
