<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Peserta;
use App\Akademik;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        // if(is_null(Auth::user()->id)){

        // }else{
             View::composer('index', function ($view) {
                $cekpeserta=Peserta::where('sts',1)->where('users_id',Auth::user()->id)->count();
                $cekakademik=Akademik::where('sts',1)->where('users_id',Auth::user()->id)->count();
                $view->with('datadiri', $cekpeserta);
                $view->with('dataakademik',$cekakademik);
            });
        //}
    }
}
