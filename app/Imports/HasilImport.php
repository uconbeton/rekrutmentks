<?php

namespace App\Imports;

use App\HasilTes;
use Maatwebsite\Excel\Concerns\ToModel;

class HasilImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $cek = HasilTes::where('users_id', $row[0])->where('tahapan_id', $row[3])->count();
        if ($cek > 0) {

        } else {
            return new HasilTes([
                'users_id' => $row[0],
                'no_tes' => $row[1],
                'email' => $row[2],
                'tahapan_id' => $row[3],
                'tanggal_tes' => $this->transformDate($row[4]),
                'tempat_tes' => $row[5],
                'jam_tes' => $row[6],
                'not' => $row[7],
                'tanggal' => date('Y-m-d'),
            ]);
        }
    }

    /**
     * Transform a date value into a Carbon object.
     *
     * @return \Carbon\Carbon|null
     */
    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
