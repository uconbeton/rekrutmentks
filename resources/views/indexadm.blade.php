@extends('app')

@section('content')
<section class="content-header">
    
    <div class="box box-widget">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                    <h3 class="box-title"><i class="fa fa-tag"></i> Color Palette</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
                <i class="fa fa-circle-o"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="input-group m-b-20">
                <input type="text" class="form-control input-white" placeholder="Cari pengetahuan disini...">
                <div class="input-group-btn">
                    <button id="button-kmks" data-ss="ZnVycW9uLmZhanJpfDB8MjAxOS0wMy0yN3wxNTowMjowNnxtaXRyYQ==" type="button" class="btn btn-inverse" data-dismiss="modal"><i class="fa fa-search"></i>&nbsp;Cari</button>
                </div>
            </div>
        </div>
    </div>
        
    
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title"><i class="fa fa-tasks"></i> Color Palette</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
                <i class="fa fa-circle-o"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <img class="img-responsive pad" src="{{url('/img/heak.png')}}" width="100%" alt="Photo">

            
        </div>
    </div>

    <div class="box box-widget">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                    <h3 class="box-title"><i class="fa fa-tasks"></i> Color Palette</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
                <i class="fa fa-circle-o"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        @can('akses hci')
            <div class="col-md-2 col-sm-6 col-xs-6 text-center" style="background: #f0f0f5;margin:1%;width:23%">
                <a href="{{url('/hci')}}" class="btn btn-block btn-white big-icon">
                    <div><img width="50%" src="{{url('/img/hci.png')}}"></div>
                    <div>HCI</div>
                </a>
            </div>
            
            
        @endcan
        @can('akses eos')
            <div class="col-md-2 col-sm-6 col-xs-6 text-center" style="background: #f0f0f5;margin:1%;width:23%">
                <a href="{{url('/smks')}}" class="btn btn-block btn-white big-icon">
                    <div><img width="50%" src="{{url('/img/eos.png')}}"></div>
                    <div>SMKS</div>
                </a>
            </div>
            
        @endcan
        @can('akses dashboard')
            <div class="col-md-2 col-sm-6 col-xs-6 text-center" style="background: #f0f0f5;margin:1%;width:23%">
                <a href="{{url('/dashboard')}}" class="btn btn-block btn-white big-icon">
                    <div><img width="50%" src="{{url('/img/dash.png')}}"></div>
                    <div>Dashboard</div>
                </a>
            </div>
           
        @endcan   
            
        </div>
    </div>
    
   
</section>
@endsection