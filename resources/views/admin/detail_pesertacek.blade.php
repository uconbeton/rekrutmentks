@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="invoice" style="background: url({{url('img/bgprof.jpg')}});background-size: 100%;">
        <div class="row">
            <div class="col-xs-12" style="text-align: center">
                    <img src="{{url('/img/ks.png')}}" width="30%">
                    <div style="margin:-20px 0px 0px 0px;width:100%;background:#dadae2;padding:3px">
                        <h4><b>DATA PESERTA REKRUTMEN</b></h4>
                    </div><br><br>
            </div>
            <!-- /.col -->
        </div>
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">
              <i class="fa fa-user"></i> Data Diri.
              <small class="pull-right">Data diri sesuai KTP</small>
            </h2>
          </div>
          <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-12 invoice-col">
            <table width="100%" border="0">
                <tr>
                    <td class="tdtd" width="25%"><b>Nama</b></td>
                    <td class="tdtd" width="2%"><b>:</b></td>
                    <td class="tdtd">{{$data->nama}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>No KTP</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->no_ktp}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>No Handphone</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->no_hp}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Email</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->userss['email']}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Alamat</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->ktp_alamat}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Provinsi</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->ktpprovs['nama']}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Kabupaten / Kota</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->ktpkabs['nama']}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Kecamatan</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->ktpkecs['nama']}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Tanggal Lahir</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->tgl_lahir}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Usia</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">...Tahun</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Jenis Kelamin</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->jenis_kelamin}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Agama</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->agama['nama_agama']}}</td>
                </tr>
                <tr>
                    <td class="tdtd"><b>Status Pernikahan</b></td>
                    <td class="tdtd"><b>:</b></td>
                    <td class="tdtd">{{$data->sts_pernikahan}}</td>
                </tr>
                
            </table><br><br>
          </div>
          <!-- /.col -->
          
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                <i class="fa fa-graduation-cap"></i> Akademik
                <small class="pull-right">Pendidikan terakhir</small>
                </h2>
            </div>
        <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-12 invoice-col">
                <table width="100%" border="0">
                    <tr>
                        <td class="tdtd" width="25%"><b>Pendidikan</b></td>
                        <td class="tdtd" width="2%"><b>:</b></td>
                        <td class="tdtd">{{$data->akademik['pendidikan']}}</td>
                    </tr>
                    <tr>
                        <td class="tdtd"><b>Nama Sekolah/Perguruan Tinggi</b></td>
                        <td class="tdtd"><b>:</b></td>
                        <td class="tdtd">{{$data->akademik['nama_sekolah']}}</td>
                    </tr>
                    <tr>
                        <td class="tdtd"><b>Jurusan</b></td>
                        <td class="tdtd"><b>:</b></td>
                        <td class="tdtd">{{$data->akademik['jurusan_sekolah']}}</td>
                    </tr>
                    <tr>
                        <td class="tdtd"><b>IPK</b></td>
                        <td class="tdtd"><b>:</b></td>
                        <td class="tdtd">{{$data->akademik['ipk']}}</td>
                    </tr>
                    
                </table><br><br>
            </div>
            <!-- /.col -->
        
        </div>
        <!-- /.row -->
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                <i class="fa fa-cubes"></i> Pengalaman Kerja
                <small class="pull-right">Pengalaman kerja dan Uraian Pekerjaan</small>
                </h2>
            </div>
        <!-- /.col -->
        
        <!-- /.row -->
        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Nama Perusahaan</th>
                <th>Jabatan</th>
                <th>Uraian Pekerjaan</th>
                <th>Lama</th>
              </tr>
              </thead>
              <tbody>
              @foreach($pengalaman as $p=>$peng)
                <tr>
                    <td width="4%">{{$p+1}}</td>
                    <td width="20%">{{$peng['nama_perusahaan']}}</td>
                    <td width="15%">{{$peng['jabatan']}}</td>
                    <td>
                        @if($peng->job1!='')
                            - {{$peng->job1}}<br>
                        @endif
                        @if($peng->job2!='')
                            - {{$peng->job2}}<br>
                        @endif
                        @if($peng->job3!='')
                            - {{$peng->job3}}<br>
                        @endif
                        @if($peng->job4!='')
                            - {{$peng->job4}}<br>
                        @endif
                        @if($peng->job5!='')
                            - {{$peng->job5}}<br>
                        @endif
                        
                    </td>
                    <td width="10%">{{$peng['selisih']}}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
  
        
  
        
        <div class="row no-print">
          <div class="col-xs-12">
            <button type="button" class="btn btn-primary pull-right">
              <i class="fa fa-download"></i> Download PDF
            </button>&nbsp;
            <button type="button" class="btn btn-default pull-right"><i class="fa fa-print"></i>Print
            </button>
            
          </div>
        </div>
      </section>
      <!-- /.content -->
  @push('datatable')
  <script>
      function terima(no){
        window.location.assign('{{url('/terima')}}/'+no);
      }

      function tolak(no){
        window.location.assign('{{url('/tolak')}}/'+no);
      }
  </script>
    
  @endpush
  @endsection