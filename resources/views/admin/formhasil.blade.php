@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <i class="fa fa-tasks"></i> Form Data Diri </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body"> 
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li style="background: transparent;border:solid #dd4b39 1px">{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif


              <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Kode :</label>
                    <input type="text" name="tahapan_id"   class="form-control" >
                  </div>
                  <div class="form-group">
                    <label>Nama Tahapan :</label>
                    <input type="text" name="nama_tahapan"   class="form-control" >
                  </div>
                  <div class="form-group">
                    <label>File Excel :</label>
                    <input type="file" name="file" class="form-control">
                  </div>
                 
                  <br><br>
                  <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </form>
            </div>
 
        </div>
    </div>

  </section>
  @push('datatable')
    <script>
        function domprov() 
          {
            window.open("{{url('/dp')}}", "list", "width=800,height=420");
          }

        function domkab() 
          {
            var id= $('#dom_prov_id').val();
            window.open("{{url('/dk/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function domkec() 
          {
            var id= $('#dom_kab_id').val();
            window.open("{{url('/dkec/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function domkel() 
          {
            var id= $('#dom_kec_id').val();
            window.open("{{url('/dkel/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpprov() 
          {
            window.open("{{url('/kp')}}", "list", "width=800,height=420");
          }

        function ktpkab() 
          {
            var id= $('#ktp_prov_id').val();
            window.open("{{url('/kk/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpkec() 
          {
            var id= $('#ktp_kab_id').val();
            window.open("{{url('/kkec/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpkel() 
          {
            var id= $('#ktp_kec_id').val();
            window.open("{{url('/kkel/')}}/"+id+"", "list", "width=800,height=420");
           
          }
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection