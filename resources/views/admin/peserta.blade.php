@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">

    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <i class="fa fa-tasks" ></i> Import File</h3>
            </div>
            
        </div>
        <div  class="box-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <div class="box-body">
                    <label>File Excel :</label><br>
                    <input type="file" name="file" class="form-control" style="width:80%;display:inline">
                    <button class="btn btn-sm btn-primary" style="margin-top:-7px"><i class="fa fa-save"></i> Import Data</button>
                    {{-- <a href="{{url('/setting/')}}"><span class="btn btn-sm btn-warning" style="margin-top:-7px"><i class="fa fa-cog"></i> Setting Data</span></a> --}}
                </div>
            </form>
        </div>
    </div>
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <i class="fa fa-tasks" ></i> Data Peserta</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
            <table id="datauser" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">User Id</th>
                        <th>No Tes</th>
                        <th>Not</th>
                        <th>Email</th>
                        <th>Tahapan</th>
                        <th>Waktu Tes</th>
                        <th>Tempat Tes</th>
                        <th>Tanggal</th>
                       
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach($user as $no=>$user)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <i class="fa fa-edit"></i>_
                                <i class="fa fa-times-circle"></i>
                            </td>
                            
                        </tr>
                    @endforeach --}}
                </tbody>
            
            </table>
              
        </div>
    </div>

  </section>
  @push('datatable')
  <script>
    $(function() {
        $('#datauser').DataTable({
            processing: true,
            serverSide: true,
            'order'   : [[ 4, "desc" ]],
            ajax: '{{url('/has')}}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'no_tes', name: 'no_tes' },
                { data: 'not', name: 'not' },
                { data: 'email', name: 'email' },
                { data: 'thp.nama', name: 'thp.nama' },
                { data: 'thp', "render": function ( data, type, row, meta ) 
                    {
                        return ' <b>Tanggal :</b> '+row.tanggal_tes+' <b>Jam :</b> '+row.jam_tes;
                    }
                },
                { data: 'tempat_tes', name: 'tempat_tes' },
                { data: 'tanggal', name: 'tanggal' }
            ]
        });
    });
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection