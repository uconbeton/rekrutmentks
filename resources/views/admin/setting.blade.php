@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">

    {{-- <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <i class="fa fa-tasks" ></i> Import File</h3>
            </div>
            
        </div>
        <div  class="box-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "">
                <div class="box-body">
                    <label>File Excel :</label><br>
                    <input type="file" name="file" class="form-control" style="width:80%;display:inline">
                    <button class="btn btn-sm btn-primary" style="margin-top:-7px"><i class="fa fa-save"></i> Import Data</button>
                    <a href="{{url('/setting/')}}"><span class="btn btn-sm btn-warning" style="margin-top:-7px"><i class="fa fa-cog"></i> Setting Data</span></a>
                </div>
            </form>
        </div>
    </div> --}}


    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <i class="fa fa-tasks" ></i> Daftar Periode Upload</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
            <table id="datauser" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Jumlah Peserta</th>
                        <th>Lulus</th>
                        <th>Gagal</th>
                        <th width="5%">ACT</th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $no=>$user)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$user->thp['nama']}}</td>
                            <td>{{$jumya->shift()['count']}} Peserta</td>
                            <td>{{$jumno->shift()['count']}} Peserta</td>
                            <td>
                            <a href="{{url('/deletesetting/'.$user->tahapan_id)}}" onclick="return confirm('Apakah yakin akan menghapus tahaan ini ?')"><span class="btn btn-sm btn-primary"><i class="fa fa-times-circle"></i> Hapus</span></a>
                            </td>
                            
                        </tr>
                    @endforeach
                </tbody>
            
            </table>
              
        </div>
    </div>

  </section>
  @push('datatable')
  <script>
    $(function() {
        $('#datauser').DataTable();
    });
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection