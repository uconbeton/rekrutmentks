@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <i class="fa fa-tasks" ></i> Data Peserta Diterima</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <input type="number" id="mulai"   class="form-control" style="width:100px;display:inline">
            <input type="number" id="sampai"   class="form-control" style="width:100px;display:inline">
            <span class="btn btn-sm btn-primary" onclick="importdata()" style="margin-top:-5px">EXPORT</span><hr>
            <b>Total Pendaftar :</b> {{$total}}<br>
            <b>Pendaftar Diterima:</b> {{$diterima}}<br>
            <b>Pendaftar Ditolak:</b> {{$ditolak}}
            <hr>
            <table id="datauser" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Nama</th>
                        <th>KTP</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kecamatan</th>
                        <th>KTP</th>
                        <th>Ijz</th>
                        <th>Trs</th>
                        <th>CV</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach($user as $no=>$user)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <i class="fa fa-edit"></i>_
                                <i class="fa fa-times-circle"></i>
                            </td>
                            
                        </tr>
                    @endforeach --}}
                </tbody>
            
            </table>
              
        </div>
    </div>

  </section>
  @push('datatable')
  <script>
    function importdata(){
        var mul=$('#mulai').val();
        var sam=$('#sampai').val();
        window.location.assign('{{url('/function/ext_peserta.php')}}?mul='+mul+'&sam='+sam);
    }
  </script>
  <script>
    
    $(function() {
        $('#datauser').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{url('/usterima')}}',
            columns: [
                { data: 'users_id', name: 'users_id' },
                { data: 'nama', name: 'nama' },
                { data: 'no_ktp', name: 'no_ktp' },
                { data: 'ktpprovs.nama', name: 'ktpprovs.nama' },
                { data: 'ktpkabs.nama', name: 'ktpkabs.nama' },
                { data: 'ktpkecs.nama', name: 'ktpkecs.nama' },
                {
                    "className": 'options',
                    "data": "filektp",
                    "render": function(data, type, full, meta){
                    
                        return '<a href="#" onclick="downloadktp(\'' +data+ '\')"><i class="fa fa-2x fa-file-image-o"></i></a>';
                        
                    }      
                },
                {
                    "className": 'options',
                    "data": "akademik.fileijazah",
                    "render": function(data, type, full, meta){
                    
                        return '<a href="#" onclick="downloadijz(\'' +data+ '\')"><i class="fa fa-2x fa-file"></i></a>';
                        
                    }      
                },
                {
                    "className": 'options',
                    "data": "akademik.filetranskip",
                    "render": function(data, type, full, meta){
                    
                        return '<a href="#" onclick="downloadtrs(\'' +data+ '\')"><i class="fa fa-2x fa-file"></i></a>';
                        
                    }      
                },
                {
                    "className": 'options',
                    "data": "no_ktp",
                    "render": function(data, type, full, meta){
                    
                        return '<a href="#" onclick="downloadcv(\'' +data+ '\')"><i class="fa fa-2x fa-file-pdf-o"></i></a>';
                        
                    }      
                },
                {
                    "className": 'options',
                    "data": "users_id",
                    "render": function(data, type, full, meta){
                    
                        return '<a href="#" onclick="view(\'' +data+ '\')"><i class="fa fa-2x fa-file-sound-o"></i></a>';
                        
                    }      
                }
            ]
        });
    });
    </script>
    <script>
        function view(a){
            window.location.assign('{{url('/detail_pesertacek/')}}/'+a);
        }
        function downloadktp(a){
            window.location.assign('{{url('/storage/')}}/'+a);
        }
        function downloadijz(a){
            window.location.assign('{{url('/storage/')}}/'+a);
        }
        function downloadcv(a){
            window.location.assign('{{url('/storage/images/CV')}}'+a+'.pdf');
        }
        function downloadtrs(a){
            window.location.assign('{{url('/storage/')}}/'+a);
        }
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection