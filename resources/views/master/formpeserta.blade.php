@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <i class="fa fa-tasks"></i> Form Data Diri </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body"> 
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li style="background: transparent;border:solid #dd4b39 1px">{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif


              <form  method="post" action="{{url('/inspeserta')}}" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type = "hidden" name = "userid" value = "{{$userid}}">          
                <div class="box-body">
                  <div class="form-group">
                    <label>Nama :</label>
                    <input type="text" name="nama" readonly  class="form-control" value="{{Auth::user()->name}}">
                  </div>
                  <div class="form-group">
                    <label>Email :</label>
                    <input type="text" name="email" readonly  class="form-control" value="{{Auth::user()->email}}">
                  </div>

                  <div class="form-group">
                    <label>No KTP:</label>
                    <input type="text" name="no_ktp"  class="form-control" readonly value="{{Auth::user()->no_ktp}}">
                  </div>
                  
                  <div class="form-group">
                    <label>File KTP:</label>
                    <input type="file" name="filektp"  class="form-control" >
                  </div>
                  @if(is_null($data->filektp)) 
                  
                  @else     
                    <div class="form-group">
                      <img src="{{url('/storage/'.$data->filektp)}}" class="imgss">
                    </div>
                  @endif
                  <input type="hidden" name="filenya" value="{{$data->filektp}}">
                  <div class="form-group">
                    <label>No Handphone:</label>
                    <input type="text" name="no_hp"  class="form-control" @if(is_null($data->no_hp))  value="{{ old('no_hp')}}" @else   value="{{$data->no_hp}}"   @endif >
                  </div>
                  <div class="form-group">
                    <label>Alamat Domisili:</label>
                    <textarea class="form-control" name="dom_alamat" >@if(is_null($data->dom_alamat)) {{ old('alamat')}} @else  {!! $data->dom_alamat !!}   @endif</textarea>
                  </div>
                  
                  <div class="form-group">
                    <label>Provinsi Domisili:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="domprov();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="domprov();"  name="dom_prov_nama" id="dom_prov_nama" value="@if(is_null($data->dom_prov_id)) {{ old('dom_prov_nama')}} @else  {{$data->domprovs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="dom_prov_id" id="dom_prov_id" value="@if(is_null($data->dom_prov_id)) {{ old('dom_prov_id')}} @else  {{$data->dom_prov_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kota / Kabupaten Domisili:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="domkab();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="domkab();"  name="dom_kab_nama" id="dom_kab_nama" value="@if(is_null($data->dom_kab_id)) {{ old('dom_kab_nama')}} @else  {{$data->domkabs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="dom_kab_id" id="dom_kab_id" value="@if(is_null($data->dom_kab_id)) {{ old('dom_kab_id')}} @else  {{$data->dom_kab_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kecamatan Domisili:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="domkec();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="domkec();"  name="dom_kec_nama" id="dom_kec_nama" value="@if(is_null($data->dom_kab_id)) {{ old('dom_kec_nama')}} @else  {{$data->domkecs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="dom_kec_id" id="dom_kec_id" value="@if(is_null($data->dom_kec_id)) {{ old('dom_kec_id')}} @else  {{$data->dom_kec_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kelurahan Domisili:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="domkel();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="domkel();"  name="dom_kel_nama" id="dom_kel_nama" value="@if(is_null($data->dom_kel_id)) {{ old('dom_kel_nama')}} @else  {{$data->domkels['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="dom_kel_id" id="dom_kel_id" value="@if(is_null($data->dom_kel_id)) {{ old('dom_kel_id')}} @else  {{$data->dom_kel_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Alamat Sesuai KTP:</label>
                    <textarea class="form-control" name="ktp_alamat" >@if(is_null($data->ktp_alamat)) {{ old('ktp_alamat')}} @else  {!! $data->ktp_alamat !!}   @endif</textarea>
                  </div>
                  
                  <div class="form-group">
                    <label>Provinsi Sesuai KTP:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="ktpprov();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="ktpprov();"  name="ktp_prov_nama" id="ktp_prov_nama" value="@if(is_null($data->ktp_prov_id)) {{ old('ktp_prov_nama')}} @else  {{$data->ktpprovs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="ktp_prov_id" id="ktp_prov_id" value="@if(is_null($data->ktp_prov_id)) {{ old('ktp_prov_id')}} @else  {{$data->ktp_prov_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kota / Kabupaten Sesuai KTP:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="ktpkab();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="ktpkab();"  name="ktp_kab_nama" id="ktp_kab_nama" value="@if(is_null($data->ktp_kab_id)) {{ old('ktp_kab_nama')}} @else  {{$data->ktpkabs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="ktp_kab_id" id="ktp_kab_id" value="@if(is_null($data->ktp_kab_id)) {{ old('ktp_kab_id')}} @else  {{$data->ktp_kab_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kecamatan Sesuai KTP:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="ktpkec();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="ktpkec();"  name="ktp_kec_nama" id="ktp_kec_nama" value="@if(is_null($data->ktp_kab_id)) {{ old('ktp_kec_nama')}} @else  {{$data->ktpkecs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="ktp_kec_id" id="ktp_kec_id" value="@if(is_null($data->ktp_kec_id)) {{ old('ktp_kec_id')}} @else  {{$data->ktp_kec_id}}   @endif">
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Kelurahan Sesuai KTP:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-search" onclick="ktpkel();"></i>
                      </div>
                      <input type="text" class="form-control pull-right" readonly onclick="ktpkel();"  name="ktp_kel_nama" id="ktp_kel_nama" value="@if(is_null($data->ktp_kel_id)) {{ old('ktp_kel_nama')}} @else  {{$data->ktpkecs['nama']}}   @endif  ">
                      <input type="hidden" class="form-control pull-right"  name="ktp_kel_id" id="ktp_kel_id" value="@if(is_null($data->ktp_kel_id)) {{ old('ktp_kel_id')}} @else  {{$data->ktp_kel_id}}   @endif">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Lahir:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker" name="tgl_lahir" value="@if(is_null($data->tgl_lahir)){{ old('tgl_lahir')}} @else  {{$data->tgl_lahir}}   @endif ">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Jenis Kelamin:</label>
                    <select name="jenis_kelamin" class="form-control">
                        <option value="">Pilih Jenis Kelamin -----</option>
                        <option value="Laki-Laki" @if($data->jenis_kelamin=='Laki-Laki') selected  @endif>Laki-Laki</option>
                        <option value="Perempuan"  @if($data->jenis_kelamin=='Perempuan') selected  @endif>Perempuan</option>
                    </select>
                   
                  </div>
                  <div class="form-group">
                    <label>Satatus Pernikahan:</label>
                    <select name="sts_pernikahan" class="form-control">
                        <option value="">Pilih Status Pernikahan -----</option>
                        <option value="Menikah" @if($data->sts_pernikahan=='Menikah') selected  @endif>Menikah</option>
                        <option value="Belum Menikah"  @if($data->sts_pernikahan=='Belum Menikah') selected  @endif>Belum Menikah</option>
                    </select>
                   
                  </div>
                  <div class="form-group">
                    <label>Agama:</label>
                    <select name="agama_id" class="form-control">
                        <option value="">Pilih Agama ----</option>
                        @foreach($agama as $agama)
                          <option value="{{$agama->id}}" @if($data->agama_id==$agama->id) selected  @endif>{{$agama->nama_agama}}</option>
                        @endforeach
                        
                     
                    </select>
                   
                  </div>
                  {{-- <div class="form-group">
                    <label>Tanggal Pemakaian:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker" name="tgl_mesin" value="@if($cek=='new') {{ old('tgl_mesin')}} @else  {{$data->tgl_mesin}}   @endif ">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Keterangan Mesin:</label><br>
                    <input type="radio"  name="sts" value="1" @if($cek=='new')  @else  @if($data->sts=='1') checked @endif  @endif   > Running
                    <input type="radio"  name="sts" value="2" @if($cek=='new')  @else  @if($data->sts=='2') checked @endif  @endif   > Stoped
                  </div> --}}
                  
                  {{-- <div class="form-group">
                    <label>Range Tanggal:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="reservation">
                    </div>
                  </div> --}}
                  <br><br>
                  <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </form>
            </div>
 
        </div>
    </div>

  </section>
  <style>
      @media screen and (max-width: 1800px) {
        .imgss {width:270px;height:100px;}
      }

      @media screen and (max-width: 500px) {
        .imgss {width:50px;height:50px;}
      } 
      /* .imgss {width:1000px;height:50px;} */
  </style>
  @push('datatable')
    <script>
        function domprov() 
          {
            window.open("{{url('/dp')}}", "list", "width=800,height=420");
          }

        function domkab() 
          {
            var id= $('#dom_prov_id').val();
            window.open("{{url('/dk/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function domkec() 
          {
            var id= $('#dom_kab_id').val();
            window.open("{{url('/dkec/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function domkel() 
          {
            var id= $('#dom_kec_id').val();
            window.open("{{url('/dkel/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpprov() 
          {
            window.open("{{url('/kp')}}", "list", "width=800,height=420");
          }

        function ktpkab() 
          {
            var id= $('#ktp_prov_id').val();
            window.open("{{url('/kk/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpkec() 
          {
            var id= $('#ktp_kab_id').val();
            window.open("{{url('/kkec/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpkel() 
          {
            var id= $('#ktp_kec_id').val();
            window.open("{{url('/kkel/')}}/"+id+"", "list", "width=800,height=420");
           
          }
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection