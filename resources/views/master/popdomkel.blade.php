@include('style')
<style>
#tabtab{
	padding:10px;
	width:100%;
	height:100%;
}td{
	font-size:12px;
	padding:0px;
}
th{
	font-size:12px;
}
</style>
<div class="box-body">
    <table id="example1"  class="table table-bordered table-striped">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th>Kelurahan </th>
				
			</tr>
		</thead>
		
		<tbody>
				@php $no = 1; @endphp
				@foreach($data as $o)
				    <tr onClick="ada('{{ $o->id_kel }}','{{ $o->nama }}')">
						<td>{{ $no++ }}</td>
						<td>{{ $o->nama }}</td>
					</tr>
				@endforeach
		</tbody>
	</table>
</div>
	
<script type="text/javascript" >

	function ada(id_kel,nama){ 
		window.opener.$('#dom_kel_id').val(id_kel);
		window.opener.$('#dom_kel_nama').val(nama);
		window.close();
	}
	
</script>


@include('script')
<script>

    $(function () {
        $('#example1').DataTable({
        'lengthChange' : true,
        'autoWidth': false,
        'paging': true,
        "columnDefs": [
            { "width": "5%", "targets": 0 }
        ]      
        })
        
    })
</script>
      