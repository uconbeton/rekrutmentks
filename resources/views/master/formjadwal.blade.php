@extends('app')

@section('content')
<?php
function bulan ($b) {
    $bulan = array ('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April',
             '05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus',
             '09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
    return $bulan[$b];
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <i class="fa fa-tasks"></i> Form Jadwal Maintenance </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body"> 
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li style="background: transparent;border:solid #dd4b39 1px">{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
                @if($not=='gagal')
                    <div class="alert alert-danger">
                        <ul>
                            
                            <li style="background: transparent;border:solid #dd4b39 1px">Data Preventive pada tanggal tersebut sudah ada</li>
                           
                        </ul>
                    </div>
                @endif

              <form  method="post" action="{{url('/insjadwal')}}">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type = "hidden" name = "cek" value = "{{$cek}}">        
                <input type = "hidden" name = "mesin_id" value = "" id="mesin_id">        
                <div class="box-body">
                    <div class="form-group">
                        <label>Seri Mesin:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-plus" onclick="mesin();"></i>
                            </div>
                            <input type="text" id="kode" readonly class="form-control pull-right"  name="kode" value="@if($cek=='new') {{ old('kode')}} @else  {{$data->kode}}   @endif ">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama Motor:</label>
                        <input type="text" id="nama" readonly class="form-control" name="nama_motor" >
                    </div>
                   <div class="form-group">
                    <label>Tanggal Preventive:</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker" name="tanggal" value="@if($cek=='new') {{ old('tgl_mesin')}} @else  {{$data->tgl_mesin}}   @endif ">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Keterangan Hasil Preventive:</label>
                    <textarea class="form-control" name="keterangan" ></textarea>
                  </div>
                  <table width="100%" border="1" class="table table-bordered table-striped">
                    <tr>
                        <th>No</th>
                        <th>Keterangan</th>
                        <th>Ceklis</th>
                    </tr>
                  @foreach($mtc as $n=>$mtc)
                    <tr>
                        <td width="5%">{{$n+1}}</td>
                        <td>{{$mtc->nama}}</td>
                        <td width="8%">
                            <input type="hidden" name="jenis_mtc_id[{{$n}}]" value="{{$mtc->id}}">
                            <input type="radio" name="cek[{{$n}}]" value="1">Yes
                            <input type="radio" name="cek[{{$n}}]" value="0" >No
                        </td>
                    </tr>
                   
                    
                  @endforeach
                  </table>
                  {{-- <div class="form-group">
                    <label>Range Tanggal:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="reservation">
                    </div>
                  </div> --}}
                  <br><br>
                  <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </form>
            </div>
 
        </div>
    </div>

  </section>
  
  @push('datatable')
    <script>
        function mesin() 
          {
             window.open("{{url('/popmesin')}}", "list", "width=800,height=420");
          }
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection