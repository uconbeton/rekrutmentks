@extends('app')

@section('content')
<?php
function bulan ($b) {
    $bulan = array ('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April',
             '05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus',
             '09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
    return $bulan[$b];
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <i class="fa fa-tasks"></i> Form Jadwal Maintenance </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body"> 
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li style="background: transparent;border:solid #dd4b39 1px">{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif


              <form  method="post" action="{{url('/insjadwal')}}">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type = "hidden" name = "mesin_id" value = "{{$act}}">        
                <div class="box-body">
                  <div class="form-group">
                    <label>Periode Bulan:</label>
                    <select name="bulan" class="form-control" >
                        
                        <option value="1" >Januari</option>
                        <option value="2" >Februari</option>
                        <option value="3" >Maret</option>
                        <option value="4" >April</option>
                        <option value="5" >Mei</option>
                        <option value="6" >Juni</option>
                        <option value="7" >Juli</option>
                        <option value="8" >Agustus</option>
                        <option value="9" >September</option>
                        <option value="10" >Oktober</option>
                        <option value="11" >November</option>
                        <option value="12" >Desember</option>
                        
                    </select>
                  </div>

                  <div class="form-group">
                        <label>Periode Tahun:</label>
                        <select class="form-control" name="tahun" >
                            @for($th=2018;$th<2040;$th++)
                                <option value="{{$th}}">{{$th}}</option>
                            @endfor
                        </select>
                  </div>

                  <div class="form-group">
                    <label>Keterangan Periode:</label>
                    <input type="text" class="form-control" name="keterangan" >
                  </div>
                  @foreach($mtc as $n=>$mtc)
                    <input name="jenis_mtc_id[]" value="{{$mtc->id}}">
                    <div class="form-group">
                        <label>{{ $mtc->nama}}:</label>
                        <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="tanggal[]" id="datepicker{{$n}}" name="tgl_mesin" >
                        </div>
                    </div>
                  @endforeach
                  {{-- <div class="form-group">
                    <label>Range Tanggal:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="reservation">
                    </div>
                  </div> --}}
                  <br><br>
                  <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </form>
            </div>
 
        </div>
    </div>

  </section>
  
  @push('datatable')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection