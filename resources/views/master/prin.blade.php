@extends('app')

@section('content')
<?php
function bulan ($b) {
    $bulan = array ('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April',
             '05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus',
             '09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
    return $bulan[$b];
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <button   class="btn btn-sm btn-success" type="submit" onclick="print('prin');"><span class="fa fa-print"></span>Cetak Audit Plan</button> </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body" id="prin"> 
                <table   width="100%" border="1" cellpadding="5" style="border-collapse:collapse;font-family:'Times New Roman', Times, serif">
                    <tr>
                        <td  class="tdr" align="center" valign="middle" width="10%"><img src="{{url('/img/ks.gif')}}" width="80%" height="70px"><br></td>
                        <td class="tdr" width="25%"><b>PT Krakatau Steel</b><br>Blast Furnance Complex</td>
                        <td class="tdr" align="center"><h3>MAINTENANCE WORK ORDER</h3><b>Preventive Maintenance</b></td>
                        <td class="tdr" width="20%"></td>
                    </tr>
                    
                </table><br>
                <table   width="100%" border="1" cellpadding="5" style="border-collapse:collapse;font-family:'Times New Roman', Times, serif">
                    <tr>
                        <td class="tdr" width="15%">MWO No.</td>
                        <td class="tdr"></td>
                        <td class="tdr" width="15%">StartDt</td>
                        <td class="tdr" width="25%"></td>
                    </tr>
                    <tr>
                        <td class="tdr">MWO Desc</td>
                        <td class="tdr"></td>
                        <td class="tdr">FinishDt</td>
                        <td class="tdr"></td>
                    </tr>
                    <tr>
                        <td class="tdr">Notif No.</td>
                        <td class="tdr"></td>
                        <td class="tdr">Stf. Hr</td>
                        <td class="tdr"></td>
                    </tr>
                    <tr>
                        <td class="tdr">Maint Plan</td>
                        <td class="tdr"></td>
                        <td class="tdr">Std. Person</td>
                        <td class="tdr"></td>
                    </tr>
                    <tr>
                        <td class="tdr">Freq</td>
                        <td class="tdr"></td>
                        <td class="tdr">Priority</td>
                        <td class="tdr"></td>
                    </tr>
                </table>
            </div>
 
        </div>
    </div>

  </section>
  <style>
        label{width:13%;background: #bff1f7;padding-left:10px;}
        .tdr{padding:5px;font-size:14;}
    </style>
  @push('datatable')
    <script>
        function mesin() 
          {
             window.open("{{url('/popmesin')}}", "list", "width=800,height=420");
          }
    </script>
    <script>
        function print(divId) {
            var content = document.getElementById(divId).innerHTML;
            var mywindow = window.open('', 'Print', 'height=600,width=1100');

            mywindow.document.write('<html><head><title></title>');
            mywindow.document.write('</head><body >');
            mywindow.document.write(content);
            mywindow.document.write('</body></html>');

            mywindow.document.close();
            mywindow.focus()
            setTimeout(function(){
                mywindow.print();
                mywindow.close();
            },250);
            return true;
        }
    </script>
  @endpush
  @endsection