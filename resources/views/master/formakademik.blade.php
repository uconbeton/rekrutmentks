@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <i class="fa fa-tasks"></i> Form Data Akademik Pendidikan Terakhir
                </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body"> 
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li style="background: transparent;border:solid #dd4b39 1px">{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif


              <form  method="post" action="{{url('/insakademik')}}" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type = "hidden" name = "userid" value = "{{$userid}}">          
                <input type = "hidden" name = "no_ktp" value = "{{Auth::user()->no_ktp}}">          
                <div class="box-body">
                  <div class="form-group">
                    <label>Pendidikan Terakhir:</label>
                    <select name="pendidikan" class="form-control">
                        <option value="">Pilih Pendidikan -----</option>
                        <option value="D3" @if($data->pendidikan=='D3') selected  @endif>D3</option>
                        <option value="S1"  @if($data->pendidikan=='S1') selected  @endif>S1</option>
                        <option value="S2"  @if($data->pendidikan=='S2') selected  @endif>S2</option>
                        <option value="S3"  @if($data->pendidikan=='S3') selected  @endif>S3</option>
                    </select>
                    
                  </div>
                  <div class="form-group">
                    <label>Nama Sekolah:</label>
                    <input type="text" name="nama_sekolah"  class="form-control"  @if(is_null($data->nama_sekolah)) value="{{ old('nama_sekolah')}}" @else  value="{{$data->nama_sekolah}}"   @endif>
                  </div>
                  <div class="form-group">
                    <label>Jurusan:</label>
                    <input type="text" name="jurusan_sekolah"  class="form-control" @if(is_null($data->jurusan_sekolah)) value="{{ old('jurusan_sekolah')}}" @else  value="{{$data->jurusan_sekolah}}"   @endif>
                  </div>
                  <div class="form-group">
                    <label>IPK :</label>
                    <input type="text" name="ipk" onkeyup="angkasaja(this);" maxlength="4" class="form-control"  @if(is_null($data->ipk)) value="{{ old('ipk')}}" @else  value="{{$data->ipk}}"   @endif>
                  </div>
                  
                  <div class="form-group">
                    <label>File Ijazah:</label>
                    <input type="file" name="fileijazah"  class="form-control" >
                  </div>
                  @if(is_null($data->fileijazah)) 
                  
                  @else     
                    <div class="form-group">
                      <a href="{{url('/storage/'.$data->fileijazah)}}" target="_blank" download><img src="{{url('/img/file.png')}}" class="imgss"></a>
                      <br>&nbsp;&nbsp;<i>File Ijazah</i>
                    </div>
                  @endif
                  <input type="hidden" name="filenyaijazah" value="{{$data->fileijazah}}">
                  <div class="form-group">
                    <label>File Transkip:</label>
                    <input type="file" name="filetranskip"  class="form-control" >
                  </div>
                  @if(is_null($data->filetranskip)) 
                  
                  @else     
                    <div class="form-group">
                      <a href="{{url('/storage/'.$data->filetranskip)}}" target="_blank" download><img src="{{url('/img/file.png')}}" class="imgss"></a>
                      <br>&nbsp;&nbsp;<i>File Transkip</i>
                    </div>
                  @endif
                  <input type="hidden" name="filenyatranskip" value="{{$data->filetranskip}}">
                  

                  
                  <br><br>
                  <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </form>
            </div>
 
        </div>
    </div>

  </section>
  <style>
      @media screen and (max-width: 1800px) {
        .imgss {width:80px;height:100px;}
      }

      @media screen and (max-width: 500px) {
        .imgss {width:50px;height:50px;}
      } 
      /* .imgss {width:1000px;height:50px;} */
  </style>
  @push('datatable')
    <script>
      function angkasaja(e) {
            if (!/^[0-9.]+$/.test(e.value)) {
            e.value ='';
            }
      }
        function domprov() 
          {
            window.open("{{url('/dp')}}", "list", "width=800,height=420");
          }

        function domkab() 
          {
            var id= $('#dom_prov_id').val();
            window.open("{{url('/dk/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function domkec() 
          {
            var id= $('#dom_kab_id').val();
            window.open("{{url('/dkec/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function domkel() 
          {
            var id= $('#dom_kec_id').val();
            window.open("{{url('/dkel/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpprov() 
          {
            window.open("{{url('/kp')}}", "list", "width=800,height=420");
          }

        function ktpkab() 
          {
            var id= $('#ktp_prov_id').val();
            window.open("{{url('/kk/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpkec() 
          {
            var id= $('#ktp_kab_id').val();
            window.open("{{url('/kkec/')}}/"+id+"", "list", "width=800,height=420");
           
          }

        function ktpkel() 
          {
            var id= $('#ktp_kec_id').val();
            window.open("{{url('/kkel/')}}/"+id+"", "list", "width=800,height=420");
           
          }
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection