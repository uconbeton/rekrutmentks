@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <a href="{{url('/formjadwal/'.$mesin->id)}}"><span class="btn-sm" style="border:1px solid #fff;color:#fff;margin:0px"><i class="fa fa-plus" ></i></span></a> List Jadwal Mesin > {{$mesin->nama}}</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Seri</th>
                        <th>Nama</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th  width="8%">sts</th>
                        <th width="8%">Cek</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $no=>$data)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$data->mstmesin['kode']}}</td>
                            <td>{{$data->mstmesin['nama']}}</td>
                            <td>{{$data->bulan}}</td>
                            <td>{{$data->tahun}}</td>
                            <td>
                                @if($data->sts==1)
                                    <span class="btn-warning" style="padding:3px;border-radius:5px"> Selesai </span>
                                @else
                                    <span class="btn-danger" style="padding:3px;border-radius:5px"> Menunggu </span>
                                @endif
                            </td>

                            <td>
                                <a href="{{url('/fjadwal/'.$data->id)}}"><span class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Detail</span></a>
                            </td>
                            
                        </tr>
                    @endforeach
                </tbody>
            
            </table>
              
        </div>
    </div>

  </section>
  @push('datatable')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection