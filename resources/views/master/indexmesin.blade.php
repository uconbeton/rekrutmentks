@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                <a href="{{url('/fmesin')}}"><span class="btn-sm" style="border:1px solid #fff;color:#fff;margin:0px"><i class="fa fa-plus" ></i></span></a> Mesin</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Seri</th>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th  width="8%">sts</th>
                        <th width="8%">Act</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $no=>$data)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$data->kode}}</td>
                            <td>{{$data->nama}}</td>
                            <td>{{$data->keterangan}}</td>
                            <td>
                                @if($data->sts==1)
                                    <span class="btn-warning" style="padding:3px;border-radius:5px"> Running </span>
                                @else
                                    <span class="btn-danger" style="padding:3px;border-radius:5px"> Stoped </span>
                                @endif
                            </td>

                            <td>
                                <a href="{{url('/fmesin/'.$data->id)}}"><i class="fa fa-edit"></i></a>_
                                <a href="{{url('/dmesin/'.$data->id)}}" onclick="return confirm('Apakah yakin untuk menghapus data ini?');"><i class="fa fa-times-circle"></i></a>
                            </td>
                            
                        </tr>
                    @endforeach
                </tbody>
            
            </table>
              
        </div>
    </div>

  </section>
  @push('datatable')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection