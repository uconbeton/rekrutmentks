@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <a href="{{url('/formjadwal/list')}}"><span class="btn-sm" style="border:1px solid #fff;color:#fff;margin:0px"><i class="fa fa-plus" ></i></span></a></span> Preventive Harian</h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Seri</th>
                        <th>Nama</th>
                        <th>Tanggal</th>
                        @foreach($mtc as $mtc)
                            <th width="5%">{{$mtc->singkatan}}</th>
                        @endforeach
                        <th width="8%">Del</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $no=>$data)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$data->mstmesin['kode']}}</td>
                            <td>{{$data->mstmesin['nama']}}</td>
                            <td>{{$data->tanggal}}</td>
                            @foreach($detmtc->where('jadwal_id',$data->id) as $o)
                                <td>
                                    @if($o->cek==1)<img src="{{url('/img/cek.png')}}" width="10px" height="10px">@else<img src="{{url('/img/not.png')}}" width="10px" height="10px">@endif
                                </td>
                            @endforeach
                            <td>
                                    <a href="{{url('/djadwal/'.$data->id)}}" onclick="return confirm('Apakah yakin untuk menghapus data ini?');"><img src="{{url('/img/del.png')}}" width="15px" height="15px"></a>_
                                    <a href="{{url('/prin/'.$data->id)}}" ><img src="{{url('/img/prin.png')}}" width="15px" height="15px"></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            
            </table>
              
        </div>
    </div>

  </section>
  @push('datatable')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection