@extends('app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="box box-widget ">
        <div class="box-header with-border" style="background: #644ad2;color:#fff">
            <div class="user-block">
                <h3 class="box-title">
                    <i class="fa fa-tasks"></i> Form Pengalaman Kerja
                </h3>
            </div>
            <!-- /.user-block -->
            <div class="box-tools">
                  
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
            <div class="box-body"> 
             
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="background: transparent;border:solid #dd4b39 1px">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

              <form  method="post" action="{{url('inspengalaman')}}" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type = "hidden" name = "userid" value = "{{$userid}}">          
                <input type = "hidden" name = "no_ktp" value = "{{Auth::user()->no_ktp}}">          
                <input type = "hidden" name = "nama_file" value = "CV{{Auth::user()->no_ktp}}">          
                <div class="box-body">
                      <div class="form-group">
                        <label>File CV:</label><br>
                        <input type="file" name="filecv"  class="form-control" accept=".pdf" >
                        @if(is_null($peserta->filecv)) 
                        
                        @else     
                          <a href="{{url('/storage/'.$peserta->filecv)}}" target="_blank" download><img src="{{url('/img/pdf.png')}}" class="imgss"></a>
                          
                        @endif
                        <input type="hidden" name="editfilecv"  class="form-control" value="{{$peserta->filecv}}" >
                      </div>
                      
                      <hr>
                      
                      @foreach($pengalaman as $p=> $peng)
                        <br><br><label>Pengalaman Kerja Ke-{{$p+1}}: <span class="btn btn-sm btn-primary" onclick="delet({{$peng['id']}})" ><i class="fa fa-remove"></i></span></label>
                        <div style="width:100%;padding:9px;margin:7px;background:#f1f1e7">
                            <div class="form-group">
                                <label>Nama Perusahaan:</label>
                                <input type="text" name="nama_perusahaan[]" value="{{$peng['nama_perusahaan']}}" class="form-control"  >
                                <label>Jabatan:</label>
                                <input type="text" name="jabatan[]"  value="{{$peng['jabatan']}}" class="form-control" >
                                <label>Lama Pengalaman:</label><br>
                                <input type="text" name="mulai[]" id="mulai'+count"  value="{{$peng['mulai']}}" class="form-control" style="width:15%;display:inline"> S/D
                                <input type="text" name="sampai[]" id="sampai'+count"  value="{{$peng['sampai']}}" onchange="selisih('+count);" class="form-control" style="width:15%;display:inline"><br>
                                <input type="hidden" name="selisih[]" id="selishnya'+count" value="{{$peng['selisih']}}" class="form-control" style="width:15%;display:inline"><br>
                                <label>Jobdes:</label><br>
                                1. <input type="text" name="job1[]" value="{{$peng['job1']}}" class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>
                                2. <input type="text" name="job2[]" value="{{$peng['job2']}}" class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>
                                3. <input type="text" name="job3[]" value="{{$peng['job3']}}" class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>
                                4. <input type="text" name="job4[]" value="{{$peng['job4']}}" class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>
                                5. <input type="text" name="job5[]" value="{{$peng['job5']}}" class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>
                                
                            </div>
                        </div>
                      @endforeach

                      <div id="container" ></div>
                      <span class="btn btn-success btn-sm" id="add" style="margin-top:30px">Tambah Pengalaman</span>
                      <input id="jum" type="text" value="0" style="height:32px;width:5px;border:solid 1px #fff;color:#fff"> 
                      <br>
                      
                  
                  
                  <br><br>
                  <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </form>
            </div>
 
        </div>
    </div>

  </section>
  <style>
      @media screen and (max-width: 1800px) {
        .imgss {width:100px;height:100px;}
      }

      @media screen and (max-width: 500px) {
        .imgss {width:50px;height:50px;}
      } 
      /* .imgss {width:1000px;height:50px;} */
  </style>



  @push('append')
  
  <script type="text/javascript">
    $(document).ready(function(e){
	    $("#fupForm").on('submit', function(e){
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: "{{url('/inspengalaman')}}",
              data: new FormData(this),
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('#submitBtn').attr("disabled","disabled");
                  $('#fupForm').css("opacity",".5");
              },
              success: function(msg){
                  alert(msg);
                  window.location.assign('{{url('/')}}');
                  $('#fupForm').css("opacity","");
                  $("#submitBtn").removeAttr("disabled");
              }
          });
        });
		});

    $(document).ready(function() {
       var count = {{$jumlah}};
        $("#add").click(function(){
          count += 1;
            $('#container').append(
              '<br><br><label>Pengalaman Kerja Ke-'+count+':</label>'
              +'<div style="width:100%;padding:9px;margin:7px;background:#f1f1e7">'
                    +'<div class="form-group">'
                        +'<label>Nama Perusahaan:</label>'
                        +'<input type="text" name="nama_perusahaan[]"  class="form-control"  >'
                        +'<label>Jabatan:</label>'
                        +'<input type="text" name="jabatan[]"  class="form-control" >'
                        +'<label>Lama Pengalaman:</label><br>'
                        +'<input type="text" name="mulai[]" id="mulai'+count+'" class="form-control" style="width:15%;display:inline"> S/D'
                        +'<input type="text" name="sampai[]" id="sampai'+count+'" onchange="selisih('+count+');" class="form-control" style="width:15%;display:inline"><br>'
                        +'<input type="hidden" name="selisih[]" id="selishnya'+count+'"  class="form-control" style="width:15%;display:inline"><br>'
                        +'<label>Jobdes:</label><br>'
                        +'1. <input type="text" name="job1[]"  class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>'
                        +'2. <input type="text" name="job2[]"  class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>'
                        +'3. <input type="text" name="job3[]"  class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>'
                        +'4. <input type="text" name="job4[]"  class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>'
                        +'5. <input type="text" name="job5[]"  class="form-control" style="width:90%;display:inline;margin-bottom:5px;"><br>'
                      
                    +'</div>'
                 +'</div>'
                  
            );
            
          $('#mulai'+count).datepicker({
              // autoclose: true
              format: 'yyyy-mm-dd'
          }); 

          $('#sampai'+count).datepicker({
              // autoclose: true
              format: 'yyyy-mm-dd'
          });   
      });
  
      $('#add').click();
      
      
      
                          
      
    });
         
    function selisih(no){
      var mulai=$('#mulai'+no).val();
      var sampai=$('#sampai'+no).val();
        $.ajax({
          type: 'GET',
          url: "{{url('/function/cekselisih.php')}}",
          data: 'mulai='+mulai+'&sampai='+sampai,
          success: function(data){
            $('#selishnya'+no).val(data);
          }
        });
     
        
    }     

    function delet(a){
        $.ajax({
          type: 'GET',
          url: "{{url('/function/deletepengalaman.php')}}",
          data: 'id='+a,
          success: function(data){
            window.location.assign('{{url('/pengalaman')}}');
          }
        });
    }           
  </script>
  @endpush
  @push('datatable')
   
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
  @endpush
  @endsection