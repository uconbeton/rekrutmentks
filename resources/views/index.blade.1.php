@extends('app')
<style>
    @media screen and (min-width: 400px) {
        .img-not{
           width:100px;
           height:100px;
        }
    }

    @media screen and (min-width: 1000px) {
        .img-not{
            width:200px;
           height:200px;
        }
    }
</style>
@section('content')
<section class="content-header">
        <div class="box box-widget">
            <div class="box-header with-border" style="background: #644ad2;color:#fff">
                <div class="user-block">
                        <h3 class="box-title"><i class="fa fa-tag"></i> Daftar Pengisian Formulir Rekrutment</h3>
                </div>
                <!-- /.user-block -->
                <div class="box-tools">
                
                
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  
                @if($datadiri==1)
                    <div class="alert alert-success alert-dismissible" style="text-align: center;">
                        <h4>
                            <a href="{{url('/peserta/')}}"><i class="icon fa fa-check"></i>Data Diri Sudah Di Isi.<br><br><img src="{{url('/img/datadiri.png')}}" class="img-not"><br>Isi form dengan benar dan sesuai</a>
                        </h4>
                    </div>

                @else
                    <div class="alert alert-info alert-dismissible" style="text-align: center;">
                        <h4>
                            <a href="{{url('/peserta/')}}"><i class="icon fa fa-warning"></i>Isi Data Diri Pekerja.<br><br><img src="{{url('/img/datadiri.png')}}" class="img-not"><br>Isi form dengan benar dan sesuai</a>
                        </h4>
                    </div>

                @endif
                @if($dataakademik==1)
                    <div class="alert alert-success alert-dismissible" style="text-align: center;">
                        <h4>
                            <a href="{{url('/akademik/')}}"><i class="icon fa fa-check"></i>Data Akademik Sudah Di Isi.<br><br><img src="{{url('/img/akademik.png')}}" class="img-not"><br>Isi form dengan benar dan sesuai</a>
                        </h4>
                    </div>

                @else
                    <div class="alert alert-info alert-dismissible" style="text-align: center;">
                        <h4>
                            <a href="{{url('/akademik/')}}"><i class="icon fa fa-warning"></i>Isi Data Akademik Pekerja.<br><br><img src="{{url('/img/akademik.png')}}" class="img-not"><br>Isi form dengan benar dan sesuai</a>
                        </h4>
                    </div>

                @endif

                @if($dataakademik==1)
                    <div class="alert alert-success alert-dismissible" style="text-align: center;">
                        <h4>
                            <a href="{{url('/akademik/')}}"><i class="icon fa fa-check"></i>Data Lowongan Sudah Di Isi.<br><br><img src="{{url('/img/job.png')}}" class="img-not"><br>Isi form dengan benar dan sesuai</a>
                        </h4>
                    </div>

                @else
                    <div class="alert alert-info alert-dismissible">
                        <h4>
                            <a href="{{url('/akademik/')}}"><i class="icon fa fa-warning"></i> Form Pengisian Data Akademik.</a>
                        </h4>
                        
                    </div>

                @endif
                
                
            </div>
        </div>
    @foreach($data  as $data)
        <div class="box box-widget">
            <div class="box-header with-border" style="background: #644ad2;color:#fff">
                <div class="user-block">
                        <h3 class="box-title"><i class="fa fa-tag"></i>  {{$data->thp['nama']}}</h3>
                </div>
                <!-- /.user-block -->
                <div class="box-tools">
               
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if($peserta->shift()['not']=='Ya')
                    Selamat anda lulus tahap {{$data->thp['nama']}}
                @else
                    Mohon Maaf anda telah gagal {{$data->thp['nama']}}
                @endif
                
            </div>
        </div>
     @endforeach   
    
    
</section>
@endsection